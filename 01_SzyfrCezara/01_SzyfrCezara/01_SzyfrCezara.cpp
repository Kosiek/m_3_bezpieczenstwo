// 01_SzyfrCezara.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "01_SzyfrCezara.h"
#include <string>
#include "utils.h"
#include <windowsx.h>
#include <Commdlg.h>

#pragma warning(disable: 4996)

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[ MAX_LOADSTRING ];                  // The title bar text
WCHAR iMainWindowClassName[ MAX_LOADSTRING ];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                RegisterMainWindowClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

// Kontrolki:
HWND labelWithCipher;
HWND encodeButton;
HWND decodeButton;
HWND fileTextBox;
HWND browseButton;

std::string encodedText;
std::string sourceText;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MY01_SZYFRCEZARA, iMainWindowClassName, MAX_LOADSTRING);
    RegisterMainWindowClass(hInstance);

    // Perform application initialization:
    if (!InitInstance(hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY01_SZYFRCEZARA));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int)msg.wParam;
}

ATOM RegisterMainWindowClass(HINSTANCE hInstance)
{
    WNDCLASS windowClass = { 0 };

    windowClass.lpfnWndProc = WndProc;
    windowClass.hInstance = hInstance;
    windowClass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    windowClass.lpszClassName = iMainWindowClassName;

    return RegisterClass(&windowClass);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
    std::wstring sample = L"sample";
    hInst = hInstance;

    HWND hWnd = CreateWindow(iMainWindowClassName, szTitle,
                             WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                            CW_USEDEFAULT, 0,
                            600, 300,
                            nullptr, nullptr, hInstance, nullptr);

    labelWithCipher = CreateWindow(L"static", sample.c_str(),
                 WS_CHILD | WS_VISIBLE | SS_LEFT,
                 20, 50, 550, 200,
                 hWnd, nullptr, NULL, NULL);

    encodeButton = CreateWindow(L"BUTTON", L"Zakoduj!",
                                WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
                                10, 10, 80, 25,
                                hWnd, nullptr,
                                nullptr,
                                nullptr);

    decodeButton = CreateWindow(L"BUTTON", L"Zdekoduj!",
                                WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
                                95, 10, 80, 25,
                                hWnd, nullptr,
                                nullptr,
                                nullptr);

    fileTextBox = CreateWindow(L"EDIT", L"Za�aduj plik...",
                 WS_CHILD | WS_VISIBLE | WS_BORDER,
                 200, 10, 300, 25,
                 hWnd, nullptr,
                 nullptr,
                 nullptr);

    Edit_Enable(fileTextBox, false);

    browseButton = CreateWindow(L"BUTTON", L"...",
                 WS_CHILD | WS_VISIBLE | WS_BORDER,
                 505, 10, 30, 25,
                 hWnd, nullptr,
                 nullptr,
                 nullptr);

    if (!hWnd)
    {
        return FALSE;
    }

    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

    return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    if (reinterpret_cast<HWND>(lParam) == browseButton)
    {
        if (LOWORD(wParam) == BN_CLICKED)
        {
            OPENFILENAME ofn = { 0 };
            ofn.lStructSize = sizeof(ofn);
            ofn.Flags |= OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_SHOWHELP;
            ofn.lpstrDefExt = L".txt";
            ofn.lpstrInitialDir = L"%UserProfile%\\Desktop\\";
            ofn.lpstrTitle = L"Otw�rz...";
            ofn.lpstrFile = new TCHAR[MAX_PATH];
            ofn.lpstrFile[0] = '\0';
            ofn.nMaxFile = MAX_PATH;

            GetOpenFileName(&ofn);
            if (ofn.lpstrFile && _tcslen(ofn.lpstrFile))
            {
                char path[MAX_PATH];
                SetWindowText(fileTextBox, ofn.lpstrFile);

                wcstombs(path, ofn.lpstrFile, _tcslen(ofn.lpstrFile) + 1);
                FILE* fileHandle = fopen(path, "r");
                sourceText.clear();
                while (!feof(fileHandle))
                {
                    char buf[64];
                    fgets(buf, 64, fileHandle);
                    sourceText += buf;
                }

                SetWindowText(labelWithCipher, std::wstring(sourceText.begin(), sourceText.end()).c_str());
            }
        }
    }

    if (reinterpret_cast<HWND>(lParam) == encodeButton)
    {
        if (LOWORD(wParam) == BN_CLICKED)
        {
            encodedText = Cezar::Encode(sourceText, 'X');
            std::wstring result(encodedText.begin(), encodedText.end());
            SetWindowText(labelWithCipher, result.c_str());
        }
    }

    if (reinterpret_cast<HWND>(lParam) == decodeButton)
    {
        if (LOWORD(wParam) == BN_CLICKED)
        {
            std::string encoded = Cezar::Decode(encodedText, 'X');
            std::wstring result(encoded.begin(), encoded.end());
            SetWindowText(labelWithCipher, result.c_str());
        }
    }

    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}