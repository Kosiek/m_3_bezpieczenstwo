#pragma once
#ifndef _UTILS_H
#define _UTILS_H

#include <string>

class Cezar
{
public:
    const static Int32 lowerA = 97;
    const static Int32 lowerZ = 122;
    const static Int32 differenceBetweenCases = 32;
    const static Int32 alphabetSize = 26;
    const static Int32 upperA = 65;
    const static Int32 upperZ = 90;

    enum WordCase
    {
        Lower = 97,
        Upper = 65
    };

    static void ConvertToCase(std::string& aText, Cezar::WordCase aCase);

    static bool IsLowerCase(const char& aCharacter)
    {
        return (aCharacter >= lowerA && aCharacter <= lowerZ)
            ? true
            : false;
    }

    static bool IsUpperCase(const char& aCharacter)
    {
        return (aCharacter >= upperA && aCharacter <= upperZ)
            ? true
            : false;
    }

    static bool IsWhitespace(const char &aKey)
    {
        return (aKey == ' ')
            ? true
            : false;
    }

    static bool IsDot(const char &aKey)
    {
        return (aKey == '.')
            ? true
            : false;
    }

    static bool IsAsterisk(const char &aKey)
    {
        return (aKey == '*')
            ? true
            : false;
    }

    static Int32 KeyToNumber(const char& aKey);

    static std::string StripString(const std::string& aInput);

    static std::string Encode(const std::string& aText, const char& aKey);

    static std::string Decode(const std::string& aText, const char& aKey);
};

#endif