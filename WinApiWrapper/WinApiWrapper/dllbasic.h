#pragma once

#ifndef _DLLBASIC_H
#define _DLLBASIC_H

#include <stdint.h>

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the WINAPIWRAPPER_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// WINAPIWRAPPER_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef WINAPIWRAPPER_EXPORTS
    #define PUBLIC_API __declspec(dllexport)
#else
    #define PUBLIC_API __declspec(dllimport)
#endif

#define Int32 int32_t
#define Uint32 uint32_t

#ifndef _WARNING
    #ifdef _MSC_VER
        #define __STR2__(x) #x
        #define __STR1__(x) __STR2__(x)
        #define _WARNING __FILE__ "("__STR1__(__LINE__)") Warning: "
    #else
        #define _WARNING(x) #warning #x
    #endif // _MSC_VER
#endif // _WARNING

#define IDS_APP_TITLE			103

#define IDR_MAINFRAME			128
#define IDD_DIALOG	            102
#define IDD_ABOUTBOX			103
#define IDM_ABOUT				104
#define IDM_EXIT				105
#define IDI_MY01			    107
#define IDI_SMALL				108
#define IDC_MY01			    109
#define IDC_MYICON				2
#ifndef IDC_STATIC
#define IDC_STATIC				-1
#endif

#endif // _DLLBASIC_H