#ifndef _WINAPIWRAPPER_STRING_H
#define _WINAPIWRAPPER_STRING_H

#include "../../dllbasic.h"

namespace WinAPIWrapper
{
    class PUBLIC_API String
    {
    public:
        String();
        ~String();

        String(const wchar_t* aText);
        String(const String& aOther);

        Uint32 Length();
        void Resize(Uint32 aNewSize);
        const wchar_t* ToCString();

        String& operator=(const wchar_t* aText);
        String& operator=(const char* aText);
        String& operator+=(const wchar_t* aText);
        String& operator+=(const char* aText);
        bool operator==(const wchar_t* aText) const;
        bool operator==(const String& aOther) const;
        wchar_t& operator[](int aIndex);

    private:
        Uint32 iMemoryLength;
        Uint32 iLength;
        wchar_t* iBuffer;

        void alloc(Uint32 aSize);
    };
}

#endif // _WINAPIWRAPPER_STRING_H
