#ifndef _WINAPIWRAPPER_CONTROLEVENTS_H
#define _WINAPIWRAPPER_CONTROLEVENTS_H

#include "../../dllbasic.h"
#include <Windows.h>

namespace WinAPIWrapper
{
    class String;

    namespace Events
    {
        struct PUBLIC_API EventArgs
        {
            HWND Handle;
            UINT Message;
            WPARAM wParam;
            LPARAM lParam;
        };

        struct PUBLIC_API EventHandler
        {
            // Provides the name for the event.
            WinAPIWrapper::String* EventName;

            // HWND to the control that the event has to belong to
            HWND ControlHandle;

            // flag of the event type, eg. BN_CLICKED
            WORD EventType;

            // Pointer to function:
            // example:
            // void browseButton_clicked(HWND aHnd, UINT aMessage, WPARAM AWParam, LPARAM aLParam)
            void(*functionHandler)(const EventArgs& aArgs);


            // Default Constructor:
            EventHandler()
            {

            }

            EventHandler(String* aEventName, 
                         HWND aControlHandle, 
                         WORD aEventType, 
                         void(*aFunctionHandler)(const EventArgs& aArgs))
            {
                EventName = aEventName;
                ControlHandle = aControlHandle;
                EventType = aEventType;
                functionHandler = aFunctionHandler;
            }
        };
    }
}

#endif //_WINAPIWRAPPER_CONTROLEVENTS_H
