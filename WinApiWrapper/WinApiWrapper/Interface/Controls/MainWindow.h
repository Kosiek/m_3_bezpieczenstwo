#ifndef _CONTROLS_MAINWINDOW_H
#define _CONTROLS_MAINWINDOW_H

#include "../../dllbasic.h"
#include "../../Interface/Tools/ControlEvents.h"
#include <Windows.h>
#include <string>
#include <vector>

#define MAX_LOADSTRING 100

namespace WinAPIWrapper
{
    namespace Controls
    {
        class MainWindowImpl;

        class PUBLIC_API MainWindow
        {
        public:
            static MainWindow* Get(_In_ HINSTANCE aProgramInstanceHandler = 0);
            HRESULT Show();
            HWND MainWindow::hWnd();

            MainWindow* SetSize(const Int32& aSizeX, const Int32& aSizeY);
            MainWindow* SetnCmdShow(const Int32& anCmdShow);

            bool RegisterEvent(Events::EventHandler* aEvent);
            bool IsEventRegistered(String* aName);
            bool UnregisterEvent(String* aName);
        private:
            
            ATOM RegisterMainWindowClass(HINSTANCE hInstance);
            static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

            MainWindow(_In_ HINSTANCE aProgramInstanceHandler);
            ~MainWindow();

        private:
            static MainWindow* iInstance;
            static MainWindowImpl* iImpl;

            WCHAR szTitle[MAX_LOADSTRING];                // The title bar text
            WCHAR iMainWindowClassName[MAX_LOADSTRING];   // the main window class name
            HINSTANCE iProgramInstance;
            HWND iMainWindowHandle;
            static bool iIsWindowClassCreated;
            Int32 inCmdShow;
            Int32 iSizeX;
            Int32 iSizeY;

            static HWND labelWithCipher;
            static HWND encodeButton;
            static HWND decodeButton;
            static HWND fileTextBox;
            static HWND browseButton;
        };
    }
}

#endif
