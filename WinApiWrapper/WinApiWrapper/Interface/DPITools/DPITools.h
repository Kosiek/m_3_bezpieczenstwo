#ifndef _DPITOOLS_H
#define _DPITOOLS_H

#include "../../dllbasic.h"
#include <ShellScalingAPI.h>
#include <Windows.h>
namespace WinAPIWrapper
{
    class PUBLIC_API DPI
    {
    public:
        static void SetDPIAwareness(_In_ const HANDLE& aHandle);
        static void GetCurrentDPIScaling(_Inout_ double& aFactorX, _Inout_ double& aFactorY);
        static void ScaleXY(_Inout_ Int32& aX, _Inout_ Int32& aY);
        static Int32 ScaleX(_In_ const Int32& aSizeX);
        static Int32 ScaleY(_In_ const Int32& aSizeY);

    private:
        static double sScaleFactorX;
        static double sScaleFactorY;
    };
}

#endif
