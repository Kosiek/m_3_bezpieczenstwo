#ifndef _WINAPIWRAPPER_MAINWINDOW_H
#define _WINAPIWRAPPER_MAINWINDOW_H

#include <vector>

#include "../../dllbasic.h"
#include "../../Interface/Tools/ControlEvents.h"

namespace WinAPIWrapper
{
    namespace Controls
    {
        typedef std::vector<Events::EventHandler*> EventList;

        class MainWindowImpl
        {
        public:
            MainWindowImpl();
            ~MainWindowImpl();

            // Returns pointer to event struct or NULL
            Events::EventHandler* GetEventMatch(HWND aControl, WORD aEvent);
            bool RegisterEvent(Events::EventHandler* aEvent);
        private:
            EventList iDefinedEvents;
        };
    }
}

#endif // _WINAPIWRAPPER_MAINWINDOW_H
