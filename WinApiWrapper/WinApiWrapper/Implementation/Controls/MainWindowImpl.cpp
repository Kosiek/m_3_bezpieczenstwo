#include "MainWindowImpl.h"
#include "../../Interface/Tools/String.h"

namespace WinAPIWrapper
{
    namespace Controls
    {
        MainWindowImpl::MainWindowImpl()
        {

        }

        MainWindowImpl::~MainWindowImpl()
        {

        }

        Events::EventHandler* MainWindowImpl::GetEventMatch(HWND aControl, WORD aEvent)
        {
            EventList::iterator it = iDefinedEvents.begin();
            const EventList::iterator end = iDefinedEvents.end();
            for (; it != end; ++it)
            {
                // If control and event match:
                if (aControl == (*it)->ControlHandle
                    && aEvent == (*it)->EventType)
                {
                    return new Events::EventHandler(**it);
                }
            }

            return nullptr;
        }

        bool MainWindowImpl::RegisterEvent(Events::EventHandler* aEvent)
        {
            iDefinedEvents.push_back(aEvent);
            return true;
        }
    }
}
