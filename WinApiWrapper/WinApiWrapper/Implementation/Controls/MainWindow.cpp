#include "../../Interface/Controls/MainWindow.h"
#include "../../Interface/DPITools/DPITools.h"
#include "../../Interface/Tools/String.h"

#include "MainWindowImpl.h"

#include <Commdlg.h>
#include <windowsx.h>
#include <Windows.h>
#include <memory>

namespace WinAPIWrapper
{
    namespace Controls
    {
        ///////////////////////////////////////////////////
        // class MainWindow                              //
        ///////////////////////////////////////////////////

        // Statics
        ///////////////////////////////////////////////////

        MainWindow* MainWindow::iInstance = nullptr;
        MainWindowImpl* MainWindow::iImpl = nullptr;
        bool MainWindow::iIsWindowClassCreated = false;

        // Construct objects
        ///////////////////////////////////////////////////

        MainWindow::MainWindow(_In_ HINSTANCE aProgramInstanceHandler)
            : iProgramInstance(aProgramInstanceHandler)
            , iMainWindowHandle(HWND())
        {
            if (!iImpl)
            {
                iImpl = new MainWindowImpl();
            }
        }

        MainWindow::~MainWindow()
        {

        }

        // Property Accessors
        ///////////////////////////////////////////////////

        MainWindow* MainWindow::Get(_In_ HINSTANCE aProgramInstanceHandler)
        {
            if (!iInstance && aProgramInstanceHandler != nullptr)
            {
                iInstance = new MainWindow(aProgramInstanceHandler);
            }

            return iInstance;
        }

        HWND MainWindow::hWnd()
        {
            return iMainWindowHandle;
        }

        MainWindow* MainWindow::SetSize(const Int32& aSizeX, const Int32& aSizeY)
        {
            iSizeX = aSizeX;
            iSizeY = aSizeY;
            return this;
        }

        MainWindow* MainWindow::SetnCmdShow(const Int32& anCmdShow)
        {
            inCmdShow = anCmdShow;
            return this;
        }

        // Functional methods
        ///////////////////////////////////////////////////

        HRESULT MainWindow::Show()
        {
            if (!iIsWindowClassCreated)
            {
                LoadStringW(iProgramInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
                LoadStringW(iProgramInstance, IDC_MY01, iMainWindowClassName, MAX_LOADSTRING);

                RegisterMainWindowClass(iProgramInstance);

                iMainWindowHandle = CreateWindow(iMainWindowClassName, szTitle,
                                                 WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                                                 CW_USEDEFAULT, 0,
                                                 DPI::ScaleX(600), DPI::ScaleY(300),
                                                 nullptr, nullptr, iProgramInstance, nullptr);

                if (!iMainWindowHandle)
                {
                    return E_FAIL;
                }

                ShowWindow(this->hWnd(), inCmdShow);
                UpdateWindow(this->hWnd());
            }

            return S_OK;
        }

        bool MainWindow::RegisterEvent(Events::EventHandler* aEvent)
        {
            return iImpl->RegisterEvent(aEvent);
        }

        bool MainWindow::IsEventRegistered(String* aName)
        {
            return false;
        }

        bool MainWindow::UnregisterEvent(String* aName)
        {
            return false;
        }

        ATOM MainWindow::RegisterMainWindowClass(HINSTANCE hInstance)
        {
            WNDCLASS windowClass = { 0 };

            windowClass.lpfnWndProc = WndProc;
            windowClass.hInstance = hInstance;
            windowClass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
            windowClass.lpszClassName = iMainWindowClassName;

            // TODO: bind iIsWindowClassCreated with result
            WORD result = RegisterClass(&windowClass);
            iIsWindowClassCreated = true;

            return result;
        }

        LRESULT CALLBACK MainWindow::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
        {
            // Process custom events if defined:
            HWND control = reinterpret_cast<HWND>(lParam);
            WORD event = LOWORD(wParam);

            Events::EventHandler* result = iImpl->GetEventMatch(control, event);
            if (result != nullptr)
            {
                result->functionHandler(Events::EventArgs { hWnd, message, wParam, lParam });
            }

            switch (message)
            {
            case WM_COMMAND:
                {
                    int wmId = LOWORD(wParam);
                    // Parse the menu selections:
                    switch (wmId)
                    {
                    case IDM_EXIT:
                        DestroyWindow(hWnd);
                        break;
                    default:
                        return DefWindowProc(hWnd, message, wParam, lParam);
                    }
                }
                break;
            case WM_PAINT:
                {
                    PAINTSTRUCT ps;
                    HDC hdc = BeginPaint(hWnd, &ps);
                    // TODO: Add any drawing code that uses hdc here...
                    EndPaint(hWnd, &ps);
                }
                break;
            case WM_DESTROY:
                PostQuitMessage(0);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
            return 0;
        }
    }
}