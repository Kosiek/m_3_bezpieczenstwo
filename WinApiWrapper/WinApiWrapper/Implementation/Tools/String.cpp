#include "../../Interface/Tools/String.h"
#include <cstring>
#include <wchar.h>
#include <stdlib.h>

#pragma warning(push)
#pragma warning(disable: 4996)

namespace WinAPIWrapper
{
    String::String()
        : iLength(0)
        , iBuffer(0)
        , iMemoryLength(0)
    {
        alloc(0);
        iBuffer[0] = L'\0';
    }

    String::~String()
    {

    }

    String::String(const wchar_t* aNewText)
        : iLength(0)
        , iBuffer(0)
        , iMemoryLength(0)
    {
        *this = aNewText;
    }

    String::String(const String& aOther)
        : iLength(0)
        , iBuffer(0)
        , iMemoryLength(0)
    {
        Uint32 length = wcslen(aOther.iBuffer);
        alloc(length);
        wcscpy(iBuffer, aOther.iBuffer);
    }

    // Public methods:
    Uint32 String::Length()
    {
        return iLength;
    }

    void String::Resize(Uint32 aNewSize)
    {

    }

    const wchar_t* String::ToCString()
    {
        return iBuffer;
    }

    // Operators:
    wchar_t& String::operator[](int aIndex)
    {
        return iBuffer[aIndex];
    }

    String& String::operator=(const wchar_t* aText)
    {
        iLength = wcslen(aText);
        alloc(iLength);
        wcscpy(iBuffer, aText);

        return *this;
    }

    String& String::operator+=(const wchar_t* aText)
    {
        Uint32 previousLength = iLength;
        iLength += wcslen(aText);
        alloc(iLength);
        wcscpy(&iBuffer[previousLength], aText);

        return *this;
    }

    bool String::operator==(const wchar_t* aText) const
    {
        int result = wcscmp(iBuffer, aText);
        return (result == 0);
    }

    bool String::operator==(const String& aOther) const
    {
        int result = wcscmp(iBuffer, aOther.iBuffer);
        return (result == 0);
    }

    String& String::operator=(const char* aText)
    {
        iLength = strlen(aText);
        if (iLength > iMemoryLength)
        {
            alloc(iLength);
        }
        mbstowcs(iBuffer, aText, iLength + 1);

        return *this;
    }

    String& String::operator+=(const char* aText)
    {
        Uint32 previousLength = iLength;
        iLength += strlen(aText);
        alloc(iLength);
        mbstowcs(&iBuffer[previousLength], aText, iLength - previousLength + 1);

        return *this;
    }

    // Private methods:

    // If there is enough memory, does nothing
    // Allocates enough memory to fit aSize characters and trailing L'\0'
    void String::alloc(Uint32 aSize)
    {
        Uint32 allocatedMemory = aSize + 1;
        if (allocatedMemory > iMemoryLength)
        {
            wchar_t* newBuf = new wchar_t[allocatedMemory];
            if (iBuffer != nullptr && iLength > 0)
            {
                wcscpy(newBuf, iBuffer);
                delete iBuffer;
            }
            iBuffer = newBuf;
            iMemoryLength = allocatedMemory;
        }

        iLength = aSize;
    }
}

#pragma warning(pop)
