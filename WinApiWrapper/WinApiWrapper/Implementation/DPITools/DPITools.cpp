#include "../../Interface/DPITools/DPITools.h"

#pragma comment(lib, "Shcore.lib")

double WinAPIWrapper::DPI::sScaleFactorX = 0.0;
double WinAPIWrapper::DPI::sScaleFactorY = 0.0;

#pragma message(_WARNING "SetDPIAwareness(): HRESULT is being cached. Please optimize.")
void WinAPIWrapper::DPI::SetDPIAwareness(_In_ const HANDLE& aHandle)
{
    HRESULT result;
    PROCESS_DPI_AWARENESS dpi = PROCESS_DPI_AWARENESS::PROCESS_DPI_UNAWARE;
    result = GetProcessDpiAwareness(aHandle, &dpi);
    if (SUCCEEDED(result) && dpi == PROCESS_DPI_AWARENESS::PROCESS_DPI_UNAWARE)
    {
        dpi = PROCESS_DPI_AWARENESS::PROCESS_PER_MONITOR_DPI_AWARE;
        SetProcessDpiAwareness(dpi);
    }
}

void WinAPIWrapper::DPI::GetCurrentDPIScaling(_Inout_ double& aFactorX, _Inout_ double& aFactorY)
{
    UINT xAxisEffectiveDPI, yAxisEffectiveDPI;
    HMONITOR currentMonitor;
    currentMonitor = MonitorFromWindow(GetActiveWindow(), MONITOR_DEFAULTTONEAREST);
    GetDpiForMonitor(currentMonitor, MONITOR_DPI_TYPE::MDT_EFFECTIVE_DPI, &xAxisEffectiveDPI, &yAxisEffectiveDPI);

    aFactorX = xAxisEffectiveDPI / 96.0;
    aFactorY = yAxisEffectiveDPI / 96.0;
}

void WinAPIWrapper::DPI::ScaleXY(_Inout_ Int32& aX, _Inout_ Int32& aY)
{
    if (!sScaleFactorX || !sScaleFactorY)
    {
        // We don't have valid DPI settings. Let's update.
        GetCurrentDPIScaling(sScaleFactorX, sScaleFactorY);
    }

    aX = static_cast<Int32>(aX * sScaleFactorX);
    aY = static_cast<Int32>(aY * sScaleFactorY);
}

Int32 WinAPIWrapper::DPI::ScaleX(_In_ const Int32& aSizeX)
{
    if (!sScaleFactorY)
    {
        // We don't have valid DPI settings. Let's update.
        GetCurrentDPIScaling(sScaleFactorX, sScaleFactorY);
    }
    return static_cast<Int32>(aSizeX * sScaleFactorX);
}

Int32 WinAPIWrapper::DPI::ScaleY(_In_ const Int32& aSizeY)
{
    if (!sScaleFactorY)
    {
        // We don't have valid DPI settings. Let's update.
        GetCurrentDPIScaling(sScaleFactorX, sScaleFactorY);
    }
    return static_cast<Int32>(aSizeY * sScaleFactorY);
}