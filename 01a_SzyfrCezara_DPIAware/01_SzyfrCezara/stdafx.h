// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#define Int32 int32_t

#ifdef _MSC_VER

#define __STR2__(x) #x
#define __STR1__(x) __STR2__(x)
#define _WARNING __FILE__ "("__STR1__(__LINE__)"): Warning: "

//#define _WARNING(x) #pragma comment _WARNINGMSG #x

#else

#define _WARNING(x) #warning #x

#endif


// TODO: reference additional headers your program requires here
