// 01_SzyfrCezara.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "01_SzyfrCezara.h"
#include <string>
#include "utils.h"
#include <WinApiWrapper/Interface/DPITools/DPITools.h>
#include <WinApiWrapper/Interface/Controls/MainWindow.h>
#include <WinApiWrapper/Interface/Tools/String.h>
#include <Windows.h>
#include <Commdlg.h>
#include <windowsx.h>

#pragma warning(disable: 4996)

std::string encodedText;
std::string sourceText;

HWND labelWithCipher;
HWND encodeButton;
HWND decodeButton;
HWND fileTextBox;
HWND browseButton;

using namespace WinAPIWrapper;

void browseButton_clicked(const Events::EventArgs& aArgs);
void encodeButton_clicked(const Events::EventArgs& aArgs);
void decodeButton_clicked(const Events::EventArgs& aArgs);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    // Test code for WinAPIWrapper::String
    String empty;
    String s = L"";
    s = "abc";
    s += " def";
    s += L" ghi.";
    wchar_t temp = s[3];
    s[3] = L'x';
    String p = s;
    if (p == s)
    {
        bool theSame = true;
    }

    Controls::MainWindow* iMainWindow = Controls::MainWindow::Get(hInstance);
    iMainWindow->SetSize(300, 300);
    iMainWindow->SetnCmdShow(nCmdShow);
    iMainWindow->Show();

    DPI::SetDPIAwareness(GetCurrentProcess());

    labelWithCipher = CreateWindow(L"static", L"",
                                   WS_CHILD | WS_VISIBLE | SS_LEFT,
                                   DPI::ScaleX(20), DPI::ScaleY(50),
                                   DPI::ScaleX(550), DPI::ScaleY(200),
                                   iMainWindow->hWnd(), nullptr, NULL, NULL);

    encodeButton = CreateWindow(L"BUTTON", L"Zakoduj!",
                                WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
                                DPI::ScaleX(10), DPI::ScaleY(10),
                                DPI::ScaleX(80), DPI::ScaleY(25),
                                iMainWindow->hWnd(),
                                nullptr, nullptr, nullptr);
    iMainWindow->RegisterEvent(
        new Events::EventHandler(new String(L"encodeButton_Clicked"),
                             encodeButton,
                             BN_CLICKED,
                             encodeButton_clicked));

    decodeButton = CreateWindow(L"BUTTON", L"Zdekoduj!",
                                WS_TABSTOP | WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
                                DPI::ScaleX(95), DPI::ScaleY(10),
                                DPI::ScaleX(80), DPI::ScaleY(25),
                                iMainWindow->hWnd(),
                                nullptr, nullptr, nullptr);
    iMainWindow->RegisterEvent(
        new Events::EventHandler(new String(L"decodeButton_Clicked"),
                             decodeButton,
                             BN_CLICKED,
                             decodeButton_clicked));

    fileTextBox = CreateWindow(L"EDIT", L"Za�aduj plik...",
                               WS_CHILD | WS_VISIBLE | WS_BORDER,
                               DPI::ScaleX(200), DPI::ScaleY(10),
                               DPI::ScaleX(300), DPI::ScaleY(25),
                               iMainWindow->hWnd(),
                               nullptr, nullptr, nullptr);

    Edit_Enable(fileTextBox, false);

    browseButton = CreateWindow(L"BUTTON", L"...",
                                WS_CHILD | WS_VISIBLE | WS_BORDER,
                                DPI::ScaleX(505), DPI::ScaleY(10),
                                DPI::ScaleX(30), DPI::ScaleY(25),
                                iMainWindow->hWnd(),
                                nullptr, nullptr, nullptr);
    iMainWindow->RegisterEvent(
        new Events::EventHandler(new String(L"browseButton_Clicked"),
                             browseButton,
                             BN_CLICKED,
                             browseButton_clicked));

    // Main message loop:
    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(109));
    MSG msg;
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int)msg.wParam;
}

void browseButton_clicked(const Events::EventArgs& aArgs)
{
    OPENFILENAME ofn = { 0 };
    ofn.lStructSize = sizeof(ofn);
    ofn.Flags |= OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_SHOWHELP;
    ofn.lpstrDefExt = L".txt";
    ofn.lpstrInitialDir = L"%UserProfile%\\Desktop\\";
    ofn.lpstrTitle = L"Otw�rz...";
    ofn.lpstrFile = new TCHAR[MAX_PATH];
    ofn.lpstrFile[0] = '\0';
    ofn.nMaxFile = MAX_PATH;

    GetOpenFileName(&ofn);
    if (ofn.lpstrFile && _tcslen(ofn.lpstrFile))
    {
        char path[MAX_PATH];
        SetWindowText(fileTextBox, ofn.lpstrFile);

        wcstombs(path, ofn.lpstrFile, _tcslen(ofn.lpstrFile) + 1);
        FILE* fileHandle = fopen(path, "r");
        sourceText.clear();
        while (!feof(fileHandle))
        {
            char buf[64];
            fgets(buf, 64, fileHandle);
            sourceText += buf;
        }

        SetWindowText(labelWithCipher, std::wstring(sourceText.begin(), sourceText.end()).c_str());
    }
}

void encodeButton_clicked(const Events::EventArgs& aArgs)
{
    encodedText = Cezar::Encode(sourceText, 'X');
    std::wstring result(encodedText.begin(), encodedText.end());
    SetWindowText(labelWithCipher, result.c_str());
}

void decodeButton_clicked(const Events::EventArgs& aArgs)
{
    std::string encoded = Cezar::Decode(encodedText, 'X');
    std::wstring result(encoded.begin(), encoded.end());
    SetWindowText(labelWithCipher, result.c_str());
}
