#include "stdafx.h"
#include "utils.h"

void Cezar::ConvertToCase(std::string& aText, Cezar::WordCase aCase)
{
    for (char &c : aText)
    {
        if (aCase = WordCase::Upper)
        {
            c = IsLowerCase(c)
                ? c - differenceBetweenCases
                : c;
        }
        else
        {
            c = IsLowerCase(c)
                ? c + differenceBetweenCases
                : c;
        }
    }
}

std::string Cezar::Encode(const std::string& aText, const char& aKey)
{
    std::string processed(aText);
    std::string result;

    ConvertToCase(processed, Cezar::WordCase::Upper);
    result = StripString(processed);
    Int32 offsetValue = KeyToNumber(aKey);

    for (char &c : result)
    {
        if (IsWhitespace(c))
        {
            c = '*';
        }
        else if (IsUpperCase(c))
        {
            c -= WordCase::Upper;
            c = (c + offsetValue) % alphabetSize;
            c += WordCase::Upper;
        }
    }

    return result;
}

std::string Cezar::Decode(const std::string& aText, const char& aKey)
{
    std::string result(aText);
    Int32 offsetValue = KeyToNumber(aKey);

    for (char &c : result)
    {
        if (IsAsterisk(c))
        {
            c = ' ';
        }
        else if (IsUpperCase(c))
        {
            c -= WordCase::Upper;
            c = (alphabetSize + c - offsetValue) % alphabetSize;
            c += WordCase::Upper;
        }
    }

    return result;
}

Int32 Cezar::KeyToNumber(const char& aKey)
{
    return IsLowerCase(aKey)
        ? aKey - lowerA
        : aKey - upperA;
}

std::string Cezar::StripString(const std::string& aInput)
{
    std::string result;
    for (int i = 0; i < aInput.size(); ++i)
    {
        const char& sign = aInput[i];
        if (IsUpperCase(sign) || IsWhitespace(sign) || IsDot(sign))
        {
            result.push_back(sign);
        }
    }

    return result;
}